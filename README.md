#  JavaScript Capstone Project
> In this project we built a movie api that displays movies to the user

# Built With

- Html
- Css
- Js
- Git & Gitflow
- webpack
- TvMaze api 



## Getting Started
To run the above project you need to meet the following requirements:


## Prerequisites
- Have [Vscode](https://code.visualstudio.com/) installed 
- Have [Git](https://git-scm.com/) installed
- Node installed

## Screenshot of our Project

<img width="1280" alt="Screen Shot 2022-04-23 at 13 23 47" src="https://user-images.githubusercontent.com/91213045/164892432-9a161551-afe5-477f-8ac2-c3b74501388b.png">

## Deployment

[link to our project](https://yannickzahinda.github.io/Javascript-group-capstone-project/)

## Run
> To run the above project :
> Run the following commands on your terminal of choice

```
git clone : https://github.com/YannickZahinda/Javascript-group-capstone-project.git

cd Javascript-group-capstone-project

 npm install
 npm run start - to spin up local webpack server
 npm run build - to build webpack
```
  


# 🤵 Author 1
- Github: https://github.com/sainingo
- https://twitter.com/saningoInn
- https://www.linkedin.com/in/saningo/
#  🤵 Author 2
- https://github.com/YannickZahinda/
- https://www.linkedin.com/in/yannick-mulikuza-846452225/
- https://twitter.com/ZahindaY
#  🤵 Author 3
- https://github.com/LYANGEND
- https://www.linkedin.com/in/david-lyangenda-623087151/
  
##  Give a ⭐ to show some 🤟
